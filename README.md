---

## Arquitetura

Devido ao meu curto período de tempo para a execução do projeto, decidi quebra-lo em duas etapas: V0 - O que eu implementei e Versão final - Como eu entendo que seria o ideal.

Na V0 foquei em realizar uma arquitetura mais simples e que eu pudesse entregar pequenos exemplos de implementação, nele eu criei apenas dois serviços: client-boundary-rest, billet-processor e transfer-processor.

O client-boundary-rest ficou responsável por atender todos os requests do usuário: Saques, depósitos, transferências e pagamento de contas.
O billet-processor ficou responsável por atender a parte assíncrona do pagamento de boleto.
O transfer-processor ficou responsável por atender a parte assíncrona da transferência de valores entre contas.


Na versão final tentei quebrar ao máximo cada funcionalidade do sistema, tornando possível o auto scaling das funcionalidades mais requisitadas e também a evolução e correção de cada serviço de forma independente. 
Nessa versão eu também criei um serviço responsável por gerenciar os "lançamentos" na conta do cliente e um outro serviço responsável pela comunicação com o cliente (SMS, email, push e etc).

**Em ambas as versões eu imaginei o Sistema de Controle de Clientes como algo externo ao escopo dessa tarefa**

Arquitetura V0 - Arquivo em /solucao-v0.PNG

Arquitetura final - Arquivo em /solucao-final.PNG

Ambos os desenhos estão no [Miro](https://miro.com/app/board/o9J_lo_kuUA=/)

---

## Comunicação assíncrona

Decidi implementar a comunicação assíncrona no endpoint de pagamento de boleto (/paymentaccount) e no de transferência (/wiretransfer). 
Nestes endpoints a aplicação receberá os dados do request + **externalId**. Ao processar o request é gerado um Id de processamento, os dados são jogados na fila responsável pelo processamento (ex: bankingbilletqueue) e é retornado um 201 + **Id da transação** para o cliente. 
No exemplo implementado (bankingbilletqueue) o request é processado, a transação é atualizada no BD e o cliente final recebe um email notificando o sucesso no processamento do boleto.

---

## Timeline com as movimentações

Tentei seguir o caminho mais simples para um V0, criei uma tabela transactions para armazenar todas as transações geradas pelo sistema e com ela eu consigo gerar o saldo, extrato e etc. 

---

## Idempotência

Para garantir a idempotência da API, adicionei o externalId em todos os requests. O externalId não pode se repetir para a mesma conta. Existem alguns pontos de melhoria para essa solução. 

---

## Banco de dados

Escolhi o Postgres para ganhar agilidade no desenvolvimento, mas acredito que para o cenário de linha do tempo a melhor opção seria um banco não relacional. De qualquer forma, o Postgres certamente seguraria a carga em uma V0 da aplicação.

---

## Logs

Para tentar facilitar a compreensão e a busca de dados no log adicionei as informações mais relevantes no MDC.

---

## Docker e Docker-compose

Padronizei o diretório do Dockerfile e docker-compose para ficarem dentro de src/main/docker.

Os modulos picpay-client-boundary-rest e picpay-banking-billet-processor geram imagens docker. Utilizei a imagem padrão da Amazon (amazoncorretto) e apenas adicionei a configuração do New Relic (APM).

O docker-compose é responsável por subir a aplicação + BD + Rabbit, adicionei configurações de CPU e memória para não degradar a máquina que estiver rodando as imagens.

---

### Contas para testes

Conta ativa = 10275396307;
Conta desativada = 10275396308;

---

## Testes unitários

Por questões de tempo para execução dessa tarefa, decidi realizar testes unitário em uma quantidade limitada de métodos e classes, em condições normais executaria testes em todas as classes, com exceção de VOs (com.araujo.picpay.client.boundary.controllers.contracts), entidades (essas seriam validadas nos testes de integração) e outras classes com apenas a responsabilidade armazenar valores, não vejo sentido de testar Get e Set.

---

## Testes funcionais

Os testes funcionais foram desenvolvidos utilizando Tests-containers, diante disso é necessário rodar um mvn clean install docker:build nos modulos picpay-client-boundary-rest e picpay-banking-billet-processor para que as imagens sejam criadas. 

Após isso, basta executar os testes e esperar a subida dos containers.

Não realizei os testes para todos os serviços expostos para os usuários por questões de tempo, porém eu seguiria a linha de testar cada endpoint validando um cenário de sucesso, um de BadRequest por erro de validação, um de falha de regra de negócio e um erro 500. Caso ocorresse um cenário mais crítico para o sistema, ele também seria incluso nos testes.