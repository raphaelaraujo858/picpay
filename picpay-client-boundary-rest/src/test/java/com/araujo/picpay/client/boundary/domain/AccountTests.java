package com.araujo.picpay.client.boundary.domain;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class AccountTests {

    @Test
    public void GivenAnAccountWithMovements_When_BalanceIsConsulted_ThenBalanceShouldReturnAsExpected(){

        // Arrange
        var expectedBalance = 500;
        var account = createAccountWithMovements();
        account.getMovements().add(createDeposit(account, 1000));
        account.getMovements().add(createWithdrawal(account, 500));

        // Act
        var balance = account.currentBalance();

        // Assert
        assertEquals(expectedBalance, balance);
    }

    @Test
    public void GivenAnAccountWithOnlyWithdrawalMovements_When_BalanceIsConsulted_ThenBalanceShouldBeNegative(){

        // Arrange
        var expectedBalance = -1500;
        var account = createAccountWithMovements();
        account.getMovements().add(createWithdrawal(account, 1000));
        account.getMovements().add(createWithdrawal(account, 500));

        // Act
        var balance = account.currentBalance();

        // Assert
        assertEquals(expectedBalance, balance);
    }

    @Test
    public void GivenAnAccountWithoutMovements_When_BalanceIsConsulted_ThenBalanceShouldZero(){

        // Arrange
        var expectedBalance = 0;
        var account = createAccountWithMovements();

        // Act
        var balance = account.currentBalance();

        // Assert
        assertEquals(expectedBalance, balance);
    }

    @Test
    public void GivenAnAccountWithMovements_When_CheckingBalanceForTransactionConsultedAboveOfBalance_ThenShouldRefuseTransaction(){

        // Arrange
        var transactionValue = 500;
        var account = createAccountWithMovements();
        account.getMovements().add(createDeposit(account, 100));

        // Act
        var response = account.haveBalanceForTransaction(transactionValue);

        // Assert
        assertFalse(response);
    }

    @Test
    public void GivenAnAccountWithMovements_When_CheckingBalanceForTransactionConsultedBelowOfBalance_ThenShouldAcceptTransaction(){

        // Arrange
        var transactionValue = 50;
        var account = createAccountWithMovements();
        account.getMovements().add(createDeposit(account, 100));

        // Act
        var response = account.haveBalanceForTransaction(transactionValue);

        // Assert
        assertTrue(response);
    }

    private Account createAccountWithMovements(){
        return new Account(123, true, 1000, 7000, 0);
    }

    private Deposit createDeposit(Account account, long amount){
        return new Deposit(account, amount, UUID.randomUUID().toString());
    }

    private Withdrawal createWithdrawal(Account account, long amount){
        return new Withdrawal(account, amount, UUID.randomUUID().toString());
    }

}
