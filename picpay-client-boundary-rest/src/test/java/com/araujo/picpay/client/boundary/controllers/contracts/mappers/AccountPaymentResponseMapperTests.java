package com.araujo.picpay.client.boundary.controllers.contracts.mappers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.araujo.picpay.client.boundary.domain.Account;
import com.araujo.picpay.client.boundary.domain.AccountPayment;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class AccountPaymentResponseMapperTests {

    @Test
    public void GivenAnAccountPayment_WhenARequestHasSuccess_ThenShouldMapAsExpected(){

        // Arrange
        var accountPayment = createAccountPayment();

        // Act
        var response = AccountPaymentResponseMapper.Map(accountPayment);

        // Assert
        assertEquals(accountPayment.getOrigin().getAccountNumber(), response.getAccountNumber());
        assertEquals(accountPayment.getAmount(), response.getAmount());
        assertEquals(accountPayment.getExternalId(), response.getTransactionExternalId());
        assertEquals(accountPayment.getId().toString(), response.getId());
        assertEquals(accountPayment.getBarCode(), response.getBarCode());
        assertEquals(accountPayment.getPaymentDate(), response.getPaymentDate());
    }

    private AccountPayment createAccountPayment(){
        return new AccountPayment(new Account(1), 100, "transactionExternalId", new Date(2022, 01, 18), "barCode");
    }
}
