package com.araujo.picpay.client.boundary.controllers.contracts.mappers;

import com.araujo.picpay.client.boundary.domain.Account;
import com.araujo.picpay.client.boundary.domain.AccountPayment;
import com.araujo.picpay.client.boundary.domain.Transaction;
import com.araujo.picpay.client.boundary.domain.Withdrawal;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GenericResponseMapperTest {

    @Test
    public void GivenATransaction_WhenARequestHasSuccess_ThenShouldMapAsExpected(){

        // Arrange
        var transaction = createAccountPayment();

        // Act
        var response = GenericResponseMapper.Map(transaction);

        // Assert
        assertEquals(transaction.getOrigin().getAccountNumber(), response.getAccountNumber());
        assertEquals(transaction.getAmount(), response.getAmount());
        assertEquals(transaction.getExternalId(), response.getTransactionExternalId());
        assertEquals(transaction.getId().toString(), response.getId());
    }

    private Transaction createAccountPayment(){
        return new Withdrawal(new Account(1), 100, "transactionExternalId");
    }

}
