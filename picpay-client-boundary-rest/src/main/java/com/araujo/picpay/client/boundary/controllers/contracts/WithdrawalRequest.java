package com.araujo.picpay.client.boundary.controllers.contracts;

public class WithdrawalRequest extends TransactionRequest {
    @Override
    public String toString() {
        return "WithdrawalRequest{" +
                "amount=" + getAmount() +
                ", transactionExternalId='" + getTransactionExternalId() + '\'' +
                '}';
    }
}
