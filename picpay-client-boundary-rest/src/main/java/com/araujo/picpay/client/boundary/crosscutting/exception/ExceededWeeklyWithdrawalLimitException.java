package com.araujo.picpay.client.boundary.crosscutting.exception;

public class ExceededWeeklyWithdrawalLimitException extends BusinessException {
    public ExceededWeeklyWithdrawalLimitException(){
        super("Exceeded weekly withdrawal limit");
    }
}
