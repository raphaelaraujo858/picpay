package com.araujo.picpay.client.boundary.domain;

public enum TransactionType {
    WITHDRAWAL,
    DEPOSIT,
    WIRE_TRANSFER,
    ACCOUNT_PAYMENT
}
