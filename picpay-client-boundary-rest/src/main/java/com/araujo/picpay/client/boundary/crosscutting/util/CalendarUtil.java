package com.araujo.picpay.client.boundary.crosscutting.util;

import java.util.Calendar;

public class CalendarUtil {
    public static boolean isSameDayOfYear(Calendar firstDate, Calendar secondDate){
        return isSame(firstDate, secondDate, Calendar.DAY_OF_YEAR);
    }

    public static boolean isSameWeekOfYear(Calendar firstDate, Calendar secondDate){
        return isSame(firstDate, secondDate, Calendar.WEEK_OF_YEAR);
    }

    private static boolean isSame(Calendar firstDate, Calendar secondDate, int fractionOfTime){
        return firstDate.get(fractionOfTime) == secondDate.get(fractionOfTime) &&
                firstDate.get(Calendar.YEAR) == secondDate.get(Calendar.YEAR);
    }
}
