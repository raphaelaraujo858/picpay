package com.araujo.picpay.client.boundary.domain;

import java.util.Calendar;
import java.util.UUID;

public class Deposit extends Transaction {

    private UUID id;
    private TransactionType type;
    private long amount;
    private Calendar createdAt;
    private Account destination;
    private String externalId;

    public Deposit(Account destination, long amount, String transactionExternalId){
        id = UUID.randomUUID();
        createdAt = Calendar.getInstance();
        type = TransactionType.DEPOSIT;
        externalId = transactionExternalId;
        this.destination = destination;
        this.amount = amount;
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public Account getOrigin() {
        return destination;
    }

    @Override
    public TransactionType getType() {
        return type;
    }

    @Override
    public long getAmount() {
        return amount;
    }

    @Override
    public Calendar getCreatedAt() {
        return createdAt;
    }
}
