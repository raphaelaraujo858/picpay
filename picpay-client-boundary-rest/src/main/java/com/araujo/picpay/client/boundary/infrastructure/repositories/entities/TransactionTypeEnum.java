package com.araujo.picpay.client.boundary.infrastructure.repositories.entities;

public enum TransactionTypeEnum {
    WITHDRAWAL,
    DEPOSIT,
    WIRE_TRANSFER,
    ACCOUNT_PAYMENT
}
