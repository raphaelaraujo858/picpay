package com.araujo.picpay.client.boundary.controllers.contracts;

public class GenericResponse {

    private long accountNumber;
    private long amount;
    private String transactionExternalId;
    private String id;

    public GenericResponse(String id, long accountNumber, long amount, String transactionExternalId) {
        this.id = id;
        this.accountNumber = accountNumber;
        this.amount = amount;
        this.transactionExternalId = transactionExternalId;
    }

    public String getId() {
        return id;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public long getAmount() {
        return amount;
    }

    public String getTransactionExternalId() {
        return transactionExternalId;
    }
}
