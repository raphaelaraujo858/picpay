package com.araujo.picpay.client.boundary.domain;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class AccountPayment extends Transaction{

    private UUID id;
    private TransactionType type;
    private long amount;
    private Calendar createdAt;
    private Account origin;
    private String externalId;
    private String barCode;
    private Date paymentDate;

    public AccountPayment(Account origin, long amount, String transactionExternalId, Date paymentDate, String barCode){
        id = UUID.randomUUID();
        createdAt = Calendar.getInstance();
        type = TransactionType.ACCOUNT_PAYMENT;
        externalId = transactionExternalId;
        this.origin = origin;
        this.amount = amount;
        this.paymentDate = paymentDate;
        this.barCode = barCode;
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public Account getOrigin() {
        return origin;
    }

    @Override
    public TransactionType getType() {
        return type;
    }

    @Override
    public long getAmount() {
        return amount;
    }

    @Override
    public Calendar getCreatedAt() {
        return createdAt;
    }

    public String getBarCode() {
        return barCode;
    }

    public Date getPaymentDate(){
        return paymentDate;
    }
}
