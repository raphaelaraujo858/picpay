package com.araujo.picpay.client.boundary.infrastructure.repositories;

import com.araujo.picpay.client.boundary.domain.AccountPayment;
import com.araujo.picpay.client.boundary.domain.Transaction;
import com.araujo.picpay.client.boundary.infrastructure.repositories.entities.TransactionEntity;
import com.araujo.picpay.client.boundary.infrastructure.repositories.entities.TransactionTypeEnum;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Calendar;

@Repository
public class AccountRepositoryImpl implements TransactionRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Transaction transaction) {
        var entity = map(transaction);
        entityManager.persist(entity);
    }

    private static TransactionEntity map(Transaction transaction){
        var entity = new TransactionEntity();
        entity.setId(transaction.getId());
        entity.setAmount(transaction.getAmount());
        entity.setOrigin(transaction.getOrigin().getAccountNumber());
        entity.setCreatedAt(transaction.getCreatedAt());
        entity.setExternalId(transaction.getExternalId());

        var databaseTransactionType = TransactionTypeEnum.valueOf(transaction.getType().name());
        entity.setTransactionType((short) databaseTransactionType.ordinal());

        if(transaction instanceof AccountPayment){
            var paymentDate = ((AccountPayment) transaction).getPaymentDate();
            if(paymentDate != null){
                var scheduleDate = Calendar.getInstance();
                scheduleDate.setTime(paymentDate);
                entity.setScheduleDate(scheduleDate);
            }

            entity.setDestination(((AccountPayment) transaction).getBarCode());
        }
        return entity;
    }
}
