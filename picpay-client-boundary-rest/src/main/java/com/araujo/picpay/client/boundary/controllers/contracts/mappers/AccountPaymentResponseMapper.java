package com.araujo.picpay.client.boundary.controllers.contracts.mappers;

import com.araujo.picpay.client.boundary.controllers.contracts.AccountPaymentResponse;
import com.araujo.picpay.client.boundary.domain.AccountPayment;

public class AccountPaymentResponseMapper {
    public static AccountPaymentResponse Map(AccountPayment request){
        return new AccountPaymentResponse(request.getId().toString(),
                request.getOrigin().getAccountNumber(),
                request.getAmount(),
                request.getExternalId(),
                request.getBarCode(),
                request.getPaymentDate());
    }
}
