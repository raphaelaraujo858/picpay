package com.araujo.picpay.client.boundary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class SpringAfterStart {

    private static Logger logger = LoggerFactory.getLogger(SpringAfterStart.class);

    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationEvent() {
        logger.info("hello world, I have just started up");
    }

}
