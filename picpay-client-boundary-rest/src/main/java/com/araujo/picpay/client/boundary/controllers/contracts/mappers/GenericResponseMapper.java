package com.araujo.picpay.client.boundary.controllers.contracts.mappers;

import com.araujo.picpay.client.boundary.controllers.contracts.GenericResponse;
import com.araujo.picpay.client.boundary.domain.Transaction;

public class GenericResponseMapper {
    public static GenericResponse Map(Transaction request){
        return new GenericResponse(request.getId().toString(), request.getOrigin().getAccountNumber(), request.getAmount(), request.getExternalId());
    }
}
