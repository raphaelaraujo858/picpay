package com.araujo.picpay.client.boundary.domain;

import java.util.Calendar;
import java.util.UUID;

public class Withdrawal extends Transaction{

    private UUID id;
    private Calendar createdAt;
    private Account origin;
    private long amount;
    private final TransactionType type;
    private String externalId;

    public Withdrawal(Account origin, long amount, String transactionExternalId){
        id = UUID.randomUUID();
        createdAt = Calendar.getInstance();
        type = TransactionType.WITHDRAWAL;
        externalId = transactionExternalId;
        this.origin = origin;
        this.amount = amount;
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public Account getOrigin() {
        return origin;
    }

    @Override
    public TransactionType getType() {
        return type;
    }

    @Override
    public long getAmount() {
        return amount;
    }

    @Override
    public Calendar getCreatedAt() {
        return createdAt;
    }
}
