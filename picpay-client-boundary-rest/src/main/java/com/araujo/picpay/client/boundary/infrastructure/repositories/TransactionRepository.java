package com.araujo.picpay.client.boundary.infrastructure.repositories;

import com.araujo.picpay.client.boundary.domain.Transaction;

public interface TransactionRepository {
    void save(Transaction transaction);
}
