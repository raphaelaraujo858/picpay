package com.araujo.picpay.client.boundary.crosscutting.exception;

public class DuplicatedTransactionExternalIdException extends BusinessException {
    public DuplicatedTransactionExternalIdException(){
        super("There is a transaction with same transactionExternalId");
    }
}
