package com.araujo.picpay.client.boundary.controllers;

import com.araujo.picpay.client.boundary.application.BankService;
import com.araujo.picpay.client.boundary.controllers.contracts.*;
import com.araujo.picpay.client.boundary.controllers.contracts.mappers.AccountPaymentResponseMapper;
import com.araujo.picpay.client.boundary.controllers.contracts.mappers.GenericResponseMapper;
import com.araujo.picpay.client.boundary.domain.Account;
import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("picpay/account/{accountNumber}/")
public class BankAccountController {

    private BankService bankService;
    private static Logger logger = LoggerFactory.getLogger(BankAccountController.class);

    @Autowired
    public BankAccountController(BankService bankService) {
        this.bankService = bankService;
    }

    //Saque
    @PostMapping(value = "/withdrawal", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity withdrawal(@PathVariable Long accountNumber, @RequestBody @Valid WithdrawalRequest request) throws Exception {

        ThreadContext.clearAll();
        logger.info("Starting withdrawal");
        ThreadContext.put("accountNumber", accountNumber.toString());
        ThreadContext.put("request", request.toString());

        var createdTransaction = bankService.withdrawal(createAccount(accountNumber), request.getAmount(), request.getTransactionExternalId());
        var response = GenericResponseMapper.Map(createdTransaction);

        ThreadContext.put("response", response.toString());
        logger.info("Finishing withdrawal");
        return ResponseEntity.ok(response);
    }

    //deposito
    @PostMapping(value = "/deposit", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity bankDeposit(@PathVariable Long accountNumber, @RequestBody @Valid BankDepositRequest request) throws Exception{

        ThreadContext.clearAll();
        logger.info("Starting bankDeposit");
        ThreadContext.put("accountNumber", accountNumber.toString());
        ThreadContext.put("request", request.toString());

        var deposit = bankService.deposit(createAccount(accountNumber), request.getAmount(), request.getTransactionExternalId());
        var response = GenericResponseMapper.Map(deposit);

        ThreadContext.put("response", response.toString());
        logger.info("Finishing bankDeposit");
        return ResponseEntity.ok(response);
    }

    //Transferencia
    //TODO Implementar se der tempo
    @PostMapping(value = "/wiretransfer", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity wireTransfer(@PathVariable(name = "accountNumber") Long originAccountNumber, @RequestBody @Valid WireTransferRequest request) throws Exception{

        var wireTransfer = bankService.wireTransfer(createAccount(originAccountNumber), createAccount(request.getDestinationAccountNumber()),
                request.getAmount(), request.getTransferDate(), request.getTransactionExternalId());
        var response = GenericResponseMapper.Map(wireTransfer);
        return ResponseEntity.ok(response);
    }

    //pagamento de contas
    @PostMapping(value = "/paymentaccount", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity paymentAccount(@PathVariable(name = "accountNumber") Long originAccountNumber, @RequestBody @Valid PaymentAccountRequest request) throws Exception{

        ThreadContext.clearAll();
        logger.info("Starting paymentAccount");
        ThreadContext.put("originAccountNumber", originAccountNumber.toString());
        ThreadContext.put("request", request.toString());

        var accountPayment = bankService.accountPayment(createAccount(originAccountNumber), request.getBarCode(), request.getAmount(), request.getPaymentDate(), request.getTransactionExternalId());
        var response = AccountPaymentResponseMapper.Map(accountPayment);

        ThreadContext.put("response", response.toString());
        logger.info("Finishing paymentAccount");
        return ResponseEntity.accepted().body(response);
    }

    private Account createAccount(Long accountNumber){
        return new Account(accountNumber);
    }
}
