package com.araujo.picpay.client.boundary.crosscutting.exception;

public class ExceededDailyWithdrawalLimitException extends BusinessException {
    public ExceededDailyWithdrawalLimitException(){
        super("Exceeded daily withdrawal limit");
    }
}
