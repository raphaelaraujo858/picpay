package com.araujo.picpay.client.boundary.controllers.contracts;

public class BankDepositRequest extends TransactionRequest{
    @Override
    public String toString() {
        return "BankDepositRequest{" +
                "amount=" + getAmount() +
                ", transactionExternalId='" + getTransactionExternalId() + '\'' +
                '}';
    }
}
