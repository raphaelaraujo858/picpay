package com.araujo.picpay.client.boundary.controllers.contracts;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class PaymentAccountRequest extends TransactionRequest{
    @NotNull
    @Size(min = 40, max = 48)
    private String barCode;

    private Date paymentDate;

    public String getBarCode() {
        return barCode;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    @Override
    public String toString() {
        return "PaymentAccountRequest{" +
                "amount=" + getAmount() +
                ", transactionExternalId='" + getTransactionExternalId() + '\'' +
                ", barCode='" + barCode + '\'' +
                ", paymentDate=" + paymentDate +
                '}';
    }
}
