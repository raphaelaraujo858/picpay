package com.araujo.picpay.client.boundary.application;

import com.araujo.picpay.client.boundary.domain.Account;
import com.araujo.picpay.client.boundary.domain.AccountPayment;
import com.araujo.picpay.client.boundary.domain.Transaction;
import com.araujo.picpay.client.boundary.infrastructure.external.services.accountpayment.AccountPaymentClient;
import com.araujo.picpay.client.boundary.infrastructure.external.services.register.RegisterClient;
import com.araujo.picpay.client.boundary.infrastructure.repositories.TransactionRepository;
import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


@Service
public class BankServiceBean implements BankService{

    @Autowired
    private TransactionRepository accountRepository;

    @Autowired
    private RegisterClient registerClient;

    @Autowired
    private AccountPaymentClient accountPaymentClient;

    private static Logger logger = LoggerFactory.getLogger(BankServiceBean.class);

    //Saque
    @Override
    @Transactional
    public Transaction withdrawal(Account account, int amount, String transactionExternalId) throws Exception {
        logger.info("Starting withdrawal");
        account = registerClient.findAccountByAccountNumber(account.getAccountNumber());
        var withdrawal = account.createWithdrawal(amount, transactionExternalId);

        accountRepository.save(withdrawal);

        ThreadContext.put("response", withdrawal.toString());
        logger.info("Finishing withdrawal");
        return withdrawal;
    }

    @Override
    @Transactional
    public Transaction deposit(Account account, int amount, String transactionExternalId) throws Exception {
        logger.info("Starting deposit");
        account = registerClient.findAccountByAccountNumber(account.getAccountNumber());
        var deposit = account.createDeposit(amount, transactionExternalId);

        accountRepository.save(deposit);

        ThreadContext.put("response", deposit.toString());
        logger.info("Finishing deposit");
        return deposit;
    }

    //TODO Implementar se der tempo
    @Override
    @Transactional
    public Transaction wireTransfer(Account origin, Account destination, int amount, Date dateTransfer, String transactionExternalId) throws Exception  {
        return null;
    }

    @Override
    @Transactional
    public AccountPayment accountPayment(Account account, String barCode, int amount, Date paymentDate, String transactionExternalId) throws Exception  {
        logger.info("Starting accountPayment");

        account = registerClient.findAccountByAccountNumber(account.getAccountNumber());

        var accountPayment = account.requestAccountPayment(barCode, amount, paymentDate, transactionExternalId);

        accountPaymentClient.requestPayment(accountPayment);
        accountRepository.save(accountPayment);

        ThreadContext.put("response", accountPayment.toString());
        logger.info("Finishing accountPayment");
        return accountPayment;
    }
}
