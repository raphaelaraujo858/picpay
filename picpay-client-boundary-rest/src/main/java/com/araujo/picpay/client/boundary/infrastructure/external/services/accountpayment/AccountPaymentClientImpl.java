package com.araujo.picpay.client.boundary.infrastructure.external.services.accountpayment;

import com.araujo.picpay.client.boundary.domain.AccountPayment;
import com.google.gson.Gson;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AccountPaymentClientImpl implements AccountPaymentClient{

    @Value("${accountpayment.queue.name}")
    private String queueName;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    private static final Gson gson = new Gson();

    @Override
    public void requestPayment(AccountPayment accountPayment) {
        rabbitTemplate.setChannelTransacted(true);
        rabbitTemplate.convertAndSend(queueName, gson.toJson(accountPayment));

    }
}
