package com.araujo.picpay.client.boundary.application;

import com.araujo.picpay.client.boundary.domain.Account;
import com.araujo.picpay.client.boundary.domain.AccountPayment;
import com.araujo.picpay.client.boundary.domain.Transaction;

import java.util.Date;

public interface BankService {
    Transaction withdrawal(Account account, int amount, String transactionExternalId) throws Exception;

    Transaction deposit(Account account, int amount, String transactionExternalId) throws Exception;

    Transaction wireTransfer(Account origin, Account destination, int amount, Date transferDate, String transactionExternalId) throws Exception;

    AccountPayment accountPayment(Account account, String barCode, int amount, Date paymentDate, String transactionExternalId) throws Exception;
}
