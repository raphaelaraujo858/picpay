package com.araujo.picpay.client.boundary.controllers.contracts;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public abstract class TransactionRequest {
    @NotNull
    @Min(1)
    private Integer amount;

    @NotBlank
    private String transactionExternalId;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getTransactionExternalId() {
        return transactionExternalId;
    }

    public void setTransactionExternalId(String transactionExternalId) {
        this.transactionExternalId = transactionExternalId;
    }
}
