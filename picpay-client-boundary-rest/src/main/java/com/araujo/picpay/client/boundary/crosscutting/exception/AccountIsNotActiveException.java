package com.araujo.picpay.client.boundary.crosscutting.exception;

public class AccountIsNotActiveException extends BusinessException {
    public AccountIsNotActiveException(){
        super("Account is not active");
    }
}
