package com.araujo.picpay.client.boundary.infrastructure.repositories;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.araujo.picpay.client.boundary.infrastructure.repositories")
public class PersistenceConfig {
}
