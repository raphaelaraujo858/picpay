package com.araujo.picpay.client.boundary.infrastructure.external.services.register;

import com.araujo.picpay.client.boundary.domain.Account;

public interface RegisterClient {
    Account findAccountByAccountNumber(long accountNumber);
}
