package com.araujo.picpay.client.boundary.domain;

import java.util.Calendar;
import java.util.UUID;

public abstract class Transaction {

    public abstract UUID getId();

    public abstract String getExternalId();

    public abstract Account getOrigin();

    public abstract TransactionType getType();

    public abstract long getAmount();

    public abstract Calendar getCreatedAt();
}
