package com.araujo.picpay.client.boundary.domain;

import com.araujo.picpay.client.boundary.crosscutting.exception.*;
import com.araujo.picpay.client.boundary.crosscutting.util.CalendarUtil;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Account {
    private final long accountNumber;
    private boolean active;
    private long dailyWithdrawalLimit;
    private long weeklyWithdrawalLimit;
    private long lisAmount;
    private transient Set<Transaction> movements;

    public Account(long accountNumber) {
        this.accountNumber = accountNumber;
        movements = new HashSet<>();
    }

    public Account(long accountNumber, boolean active, long dailyWithdrawalLimit, long weeklyWithdrawalLimit, long lisAmount) {
        this(accountNumber);
        this.active = active;
        this.dailyWithdrawalLimit = dailyWithdrawalLimit;
        this.weeklyWithdrawalLimit = weeklyWithdrawalLimit;
        this.lisAmount = lisAmount;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public boolean isActive() {
        return active;
    }

    public long getDailyWithdrawalLimit() {
        return dailyWithdrawalLimit;
    }

    public long getWeeklyWithdrawalLimit() {
        return weeklyWithdrawalLimit;
    }

    public long getLisAmount() {
        return lisAmount;
    }

    public Set<Transaction> getMovements() {
        return movements;
    }

    public void setMovements(Set<Transaction> movements) {
        this.movements = movements;
    }

    //TODO jogar essa operacao para o banco e colocar pontos de resumo
    //TODO aceitar WireTransfer como débito
    public long currentBalance(){
        var credits = (Long) movements.stream()
                .filter(t -> t.getType().equals(TransactionType.DEPOSIT)).mapToLong(Transaction::getAmount).sum();

        var debts = (Long) movements.stream()
                .filter(t -> !t.getType().equals(TransactionType.DEPOSIT)).mapToLong(Transaction::getAmount).sum();

        return credits - debts;
    }

    public boolean haveBalanceForTransaction(long transactionAmount){
        return currentBalance() + getLisAmount() > transactionAmount;
    }

    public Withdrawal createWithdrawal(long amount, String transactionExternalId) throws Exception{
        if(!isActive()){
            throw new AccountIsNotActiveException();
        }

        if(isADuplicatedTransaction(TransactionType.WITHDRAWAL, transactionExternalId)){
            throw new DuplicatedTransactionExternalIdException();
        }

        if(!haveBalanceForTransaction(amount)){
            throw new InsufficientBalanceException();
        }

        if(totDailyWithdrawal() + amount > getDailyWithdrawalLimit()){
            throw new ExceededDailyWithdrawalLimitException();
        }

        if(totWeeklyWithdrawals() + amount > getWeeklyWithdrawalLimit()){
            throw new ExceededWeeklyWithdrawalLimitException();
        }

        var withdrawal = new Withdrawal(this, amount, transactionExternalId);
        movements.add(withdrawal);

        return withdrawal;
    }

    public Deposit createDeposit(long amount, String transactionExternalId) throws Exception{
        if(!isActive()){
            throw new AccountIsNotActiveException();
        }

        if(isADuplicatedTransaction(TransactionType.DEPOSIT, transactionExternalId)){
            throw new DuplicatedTransactionExternalIdException();
        }

        var deposit = new Deposit(this, amount, transactionExternalId);
        movements.add(deposit);

        return deposit;
    }

    public AccountPayment requestAccountPayment(String barCode, int amount, Date paymentDate, String transactionExternalId) throws Exception{
        if(!isActive()){
            throw new AccountIsNotActiveException();
        }

        if(!haveBalanceForTransaction(amount)){
            throw new InsufficientBalanceException();
        }

        if(isADuplicatedTransaction(TransactionType.ACCOUNT_PAYMENT, transactionExternalId)){
            throw new DuplicatedTransactionExternalIdException();
        }

        var accountPayment = new AccountPayment(this, amount, transactionExternalId, paymentDate, barCode);
        movements.add(accountPayment);

        return accountPayment;
    }

    public long totDailyWithdrawal(){
        var currentDay = Calendar.getInstance();
        return movements.stream()
                .filter(t -> CalendarUtil.isSameDayOfYear(t.getCreatedAt(), currentDay))
                .filter(t -> t.getType().equals(TransactionType.WITHDRAWAL)).mapToLong(Transaction::getAmount).sum();
    }

    public long totWeeklyWithdrawals(){
        var currentDay = Calendar.getInstance();
        return movements.stream()
                .filter(t -> CalendarUtil.isSameWeekOfYear(t.getCreatedAt(), currentDay))
                .filter(t -> t.getType().equals(TransactionType.WITHDRAWAL)).mapToLong(Transaction::getAmount).sum();
    }

    public boolean isADuplicatedTransaction(TransactionType transactionType, String transactionExternalId){
        return movements.stream().anyMatch(t -> t.getExternalId().equals(transactionExternalId) && t.getType().equals(transactionType));
    }
}