package com.araujo.picpay.client.boundary.controllers.contracts;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class WireTransferRequest extends TransactionRequest{
    @NotNull
    @Min(1)
    @Max(99999999)
    private Long destinationAccountNumber;

    private Date transferDate;

    public Long getDestinationAccountNumber() {
        return destinationAccountNumber;
    }

    public void setDestinationAccountNumber(Long destinationAccountNumber) {
        this.destinationAccountNumber = destinationAccountNumber;
    }

    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }
}
