package com.araujo.picpay.client.boundary.crosscutting.exception;

public class BusinessException extends Exception{
    public BusinessException(String message){
        super(message);
    }
}
