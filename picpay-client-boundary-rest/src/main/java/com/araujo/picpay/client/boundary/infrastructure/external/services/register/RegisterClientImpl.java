package com.araujo.picpay.client.boundary.infrastructure.external.services.register;

import com.araujo.picpay.client.boundary.domain.Account;
import com.araujo.picpay.client.boundary.domain.Deposit;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import java.util.UUID;

@Component
public class RegisterClientImpl implements RegisterClient {

    private static Logger logger = LoggerFactory.getLogger(RegisterClientImpl.class);
    private static int count = 0;

    //Todo Parte simulada
    @Override
    @CircuitBreaker(name = "findAccountByAccountNumber", fallbackMethod = "fallbackAccountByAccountNumber")
    public Account findAccountByAccountNumber(long accountNumber){
        var account = new Account(accountNumber, accountNumber != 10275396308l, 1000, 7000, 0);
        var initialBalance = new Deposit(account, 1000, UUID.randomUUID().toString());
        account.getMovements().add(initialBalance);

        logger.info("Tentando: " + ++count);

        switch (Long.toString(accountNumber)){
            case "123": throw new NullPointerException();
            case "124": throw new RestClientException(null);
        }

        return account;
    }

    public Account fallbackAccountByAccountNumber(long accountNumber, Throwable throwable){
        logger.info("Fallback");
        return new Account(123);
    }

}
