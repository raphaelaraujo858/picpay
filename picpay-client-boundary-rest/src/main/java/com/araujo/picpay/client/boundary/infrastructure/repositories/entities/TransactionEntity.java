package com.araujo.picpay.client.boundary.infrastructure.repositories.entities;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="TRANSACTION")
public class TransactionEntity {
    @Id
    @Column(name="id", length=36, nullable=false, unique=true)
    private UUID id;

    @Column(name="external_id", length=50, nullable=false, unique=false)
    private String externalId;

    @Column(name="origin", nullable=false, unique=false)
    private long origin;

    @Column(name="destination", length=50, nullable=true, unique=false)
    private String destination;

    @Column(name="transaction_type", nullable=false, unique=false)
    private short transactionType;

    private long amount;

    @Column(name="created_at", nullable=false, unique=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar createdAt;

    @Column(name="schedule_date", nullable=true, unique=false)
    @Temporal(TemporalType.DATE)
    private Calendar scheduleDate;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public long getOrigin() {
        return origin;
    }

    public void setOrigin(long origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public short getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(short transactionType) {
        this.transactionType = transactionType;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public Calendar getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Calendar createdAt) {
        this.createdAt = createdAt;
    }

    public Calendar getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(Calendar scheduleDate) {
        this.scheduleDate = scheduleDate;
    }
}
