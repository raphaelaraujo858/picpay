package com.araujo.picpay.client.boundary.controllers;

import com.araujo.picpay.client.boundary.crosscutting.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalHandlerException {

    private static Logger logger = LoggerFactory.getLogger(GlobalHandlerException.class);
    private static final String MSG_FALHA_PROC = "Falha de processamento";

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity handleValidationExceptions(MethodArgumentNotValidException ex) {
        var errors = new HashMap<String, Object>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return buildResponseWithJson(HttpStatus.BAD_REQUEST, errors);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BusinessException.class)
    public ResponseEntity handleBusinessException(BusinessException ex) {
        var errors = new HashMap<String, Object>();
        errors.put("error", ex.getMessage());
        return buildResponseWithJson(HttpStatus.BAD_REQUEST, errors);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception ex) {
        logger.error("Exception nao capturada", ex);
        var errors = new HashMap<String, Object>();
        errors.put("error", MSG_FALHA_PROC);
        return buildResponseWithJson(HttpStatus.INTERNAL_SERVER_ERROR, errors);
    }

    private ResponseEntity buildResponseWithJson(HttpStatus status, Map<String, Object> response){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity(response, headers, status);
    }

}
