package com.araujo.picpay.client.boundary.controllers.contracts;

import java.util.Date;

public class AccountPaymentResponse extends GenericResponse{

    private String barCode;
    private Date paymentDate;

    public AccountPaymentResponse(String id, long accountNumber, long amount, String transactionExternalId, String barCode, Date paymentDate) {
        super(id, accountNumber, amount, transactionExternalId);
        this.barCode = barCode;
        this.paymentDate = paymentDate;
    }

    public String getBarCode() {
        return barCode;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    @Override
    public String toString() {
        return "AccountPaymentResponse{" +
                "barCode='" + barCode + '\'' +
                ", id='" + getId() + '\'' +
                ", accountNumber=" + getAccountNumber() +
                ", amount=" + getAmount() +
                ", transactionExternalId='" + getTransactionExternalId() + '\'' +
                ", paymentDate='" + paymentDate + '\'' +
                '}';
    }
}
