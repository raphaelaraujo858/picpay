package com.araujo.picpay.client.boundary.infrastructure.external.services.accountpayment;

import com.araujo.picpay.client.boundary.domain.AccountPayment;

public interface AccountPaymentClient {

    void requestPayment(AccountPayment accountPayment);

}
