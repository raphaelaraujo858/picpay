package com.araujo.picpay.client.boundary.crosscutting.exception;

public class InsufficientBalanceException extends BusinessException {
    public InsufficientBalanceException(){
        super("There is no balance for withdrawal");
    }
}
