package com.araujo.picpay;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import org.junit.jupiter.api.Test;
import java.util.Map;

class DepositTests extends BaseTests {

	@Test
	public void GivenAnActiveAccount_WhenDoAValidDeposit_ThenDepositOccursWithSuccess() {

		// Arrange
		int amount = 100;
		String externalId = createExternalId();
		var body = createDebitBody(amount, externalId);

		// act
		var response = executeDepositRequest(body, getActiveAccountNumber());

		// assert
		var responseStatusCode = response.get("status");
		var responseBody = (Map) response.get("body");

		assertEquals(OK, responseStatusCode);
		assertEquals(amount, responseBody.get("amount"));
		assertEquals(externalId, responseBody.get("transactionExternalId"));
		assertEquals(getActiveAccountNumber(), responseBody.get("accountNumber"));
		assertTrue(responseBody.containsKey("id"));

		//TODO adicionar validações de informações no BD
	}

	@Test
	public void GivenAInactiveAccount_WhenDoAValidDeposit_ThenTransactionShouldBeRefused() {

		// Arrange
		int amount = 100;
		String externalId = createExternalId();
		String expectedErrorMessage = "Account is not active";
		var body = createDebitBody(amount, externalId);

		// act
		var response = executeDepositRequest(body, getInactiveAccountNumber());

		// assert
		var responseStatusCode = response.get("status");
		var responseBody = (Map) response.get("body");

		assertEquals(BAD_REQUEST, responseStatusCode);
		assertEquals(expectedErrorMessage, responseBody.get("error"));
	}

	@Test
	public void GivenAnAccount_WhenDoADepositWithNegativeAmount_ThenTransactionShouldBeRefused() {

		// Arrange
		int amount = -100;
		String externalId = createExternalId();
		String expectedErrorMessage = "must be greater than or equal to 1";
		var body = createDebitBody(amount, externalId);

		// act
		var response = executeDepositRequest(body, getActiveAccountNumber());

		// assert
		var responseStatusCode = response.get("status");
		var responseBody = (Map) response.get("body");

		assertEquals(BAD_REQUEST, responseStatusCode);
		assertEquals(expectedErrorMessage, responseBody.get("amount"));
	}

}
