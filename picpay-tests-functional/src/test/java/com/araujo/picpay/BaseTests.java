package com.araujo.picpay;

import com.araujo.picpay.infra.BasicInfra;
import com.google.gson.Gson;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BaseTests extends BasicInfra {

    private RestTemplate restTemplate = new RestTemplate();
    private String depositPath = "/deposit";
    private String paymentAccountPath = "/paymentaccount";
    private String clientRestContext = "http://" + getPicpayClientBoundaryRestContainer().getContainerIpAddress() + ":" +
			getPicpayClientBoundaryRestContainer().getFirstMappedPort() + "/picpay/account/{accountNumber}";
    //private String clientRestContext = "http://localhost:8080" + "/picpay/account/{accountNumber}";
    private Long activeAccountNumber = 10275396307l;
    private Long inactiveAccountNumber = 10275396308l;

    private Gson gson = new Gson();

    public RestTemplate getRestTemplate(){
        return restTemplate;
    }

    public String getClientRestContext() {
        return clientRestContext;
    }

    public Long getActiveAccountNumber() {
        return activeAccountNumber;
    }

    public Long getInactiveAccountNumber() {
        return inactiveAccountNumber;
    }

    public Gson getGson() {
        return gson;
    }

    public String createExternalId(){
        return UUID.randomUUID().toString();
    }

    public String createDebitBody(int amount, String externalId){
        var request = new HashMap<String, Object>();
        request.put("amount", amount);
        request.put("transactionExternalId", externalId);
        return getGson().toJson(request);
    }

    public String createPaymentAccountBody(int amount, String externalId, String barCode, Date paymentDate){
        var request = new HashMap<String, Object>();
        request.put("amount", amount);
        request.put("transactionExternalId", externalId);
        request.put("barCode", barCode);
        request.put("paymentDate", formatDateToJsonPattern(paymentDate));
        return getGson().toJson(request);
    }

    public Map<String, Object> executeDepositRequest(String body, Long accountNumber){
        return executePost(getClientRestContext() + depositPath, body, accountNumber);
    }

    public Map<String, Object> executePaymentAccountRequest(String body, Long accountNumber){
        return executePost(getClientRestContext() + paymentAccountPath, body, accountNumber);
    }

    private Map<String, Object> executePost(String url, String body, Long accountNumber){
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        var request = new HttpEntity<String>(body, headers);

        var responseMap = new HashMap<String, Object>();

        try {
            var response =  getRestTemplate().postForEntity(url, request, HashMap.class, accountNumber.toString());
            responseMap.put("status", response.getStatusCode());
            responseMap.put("body", response.getBody());

        }catch (HttpClientErrorException ex){
            responseMap.put("status", ex.getStatusCode());
            var responseBody = getGson().fromJson(ex.getResponseBodyAsString(), Map.class);
            responseMap.put("body", responseBody);
        }

        return responseMap;
    }

    public String formatDateToJsonPattern(Date date){
        var sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
}
