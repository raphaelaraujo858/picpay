package com.araujo.picpay.infra.container;

import com.github.dockerjava.api.command.CreateContainerCmd;
import org.testcontainers.containers.GenericContainer;

import java.time.Duration;
import java.util.Arrays;

public class RabbitMqContainer extends GenericContainer {

    public static final Integer PORT = 5672;
    public static final String IMAGE = "rabbitmq:3.9.12-alpine";

    public RabbitMqContainer(){
        super(IMAGE);

        int startupTimeoutInMinutes = 1;

        setPortBindings(Arrays.asList(PORT + ":" + PORT));
        addExposedPort(PORT);
        withExposedPorts(PORT);
        withStartupTimeout(Duration.ofMinutes(startupTimeoutInMinutes));
        withNetworkAliases("picpay");
        withEnv("RABBITMQ_DEFAULT_USER", "user");
        withEnv("RABBITMQ_DEFAULT_PASS", "password");
        withNetworkAliases("rabbit.nohost");

        withCreateContainerCmdModifier(cmd -> ((CreateContainerCmd) cmd).getHostConfig()
                .withMemory(getMemory())
                .withCpusetCpus("1")
                .withCpuShares(800));
    }

    public String getAddress() {
        return getContainerIpAddress() + ":" + getFirstMappedPort();
    }

    private Long getMemory() {
        var memory = (400 * 1024 * 1024);
        return (long) memory;
    }
}
