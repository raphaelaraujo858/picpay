package com.araujo.picpay.infra.container;

import com.github.dockerjava.api.command.CreateContainerCmd;
import org.testcontainers.containers.GenericContainer;

import java.time.Duration;
import java.util.Arrays;

public class PostgresContainer extends GenericContainer {

    public static final Integer PORT = 5432;
    public static final String IMAGE = "postgres:alpine3.15";

    public PostgresContainer(){
        super(IMAGE);

        int startupTimeoutInMinutes = 1;

        setPortBindings(Arrays.asList(PORT + ":" + PORT));
        addExposedPort(PORT);
        withExposedPorts(PORT);
        withStartupTimeout(Duration.ofMinutes(startupTimeoutInMinutes));
        withNetworkAliases("picpay");
        withEnv("POSTGRES_USER", "postgres");
        withEnv("POSTGRES_PASSWORD", "password");
        withEnv("POSTGRES_DB", "picpay");
        withNetworkAliases("db.nohost");

        withCreateContainerCmdModifier(cmd -> ((CreateContainerCmd) cmd).getHostConfig()
                .withMemory(getMemory())
                .withCpusetCpus("1")
                .withCpuShares(800));
    }

    public String getAddress() {
        return getContainerIpAddress() + ":" + getFirstMappedPort();
    }

    private Long getMemory() {
        var memory = (400 * 1024 * 1024);
        return (long) memory;
    }
}
