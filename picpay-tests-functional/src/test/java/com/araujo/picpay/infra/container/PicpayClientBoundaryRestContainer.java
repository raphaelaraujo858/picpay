package com.araujo.picpay.infra.container;

import com.github.dockerjava.api.command.CreateContainerCmd;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.HttpWaitStrategy;
import org.testcontainers.containers.wait.strategy.Wait;

import java.time.Duration;
import java.util.Arrays;

public class PicpayClientBoundaryRestContainer extends GenericContainer {

    public static final Integer PORT = 8080;
    public static final String IMAGE = "leorapha/picpay-client-boundary-rest:0.0.1-snapshot";

    public PicpayClientBoundaryRestContainer(){
        super(IMAGE);

        int startupTimeoutInMinutes = 3;

        setPortBindings(Arrays.asList(PORT + ":" + PORT));
        addExposedPort(PORT);
        withExposedPorts(PORT);
        withStartupTimeout(Duration.ofMinutes(startupTimeoutInMinutes));
        withNetworkAliases("picpay");
        withEnv("SPRING_PROFILES_ACTIVE", "dev");
        withEnv("NEWRELIC_ENVIRONMENT", "development");


        withCreateContainerCmdModifier(cmd -> ((CreateContainerCmd) cmd).getHostConfig()
                .withMemory(getMemory())
                .withCpusetCpus("2")
                .withCpuShares(800));

        try {
            HttpWaitStrategy wait = Wait.forHttp("/management/health");
            wait.withStartupTimeout(Duration.ofMinutes(startupTimeoutInMinutes));
            waitingFor(wait);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getAddress() {
        return getContainerIpAddress() + ":" + getFirstMappedPort();
    }

    private Long getMemory() {
        var memory = (800 * 1024 * 1024);
        return (long) memory;
    }

}
