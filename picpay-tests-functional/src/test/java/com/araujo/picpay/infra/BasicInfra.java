package com.araujo.picpay.infra;

import com.araujo.picpay.infra.container.PicpayClientBoundaryRestContainer;
import com.araujo.picpay.infra.container.PostgresContainer;
import com.araujo.picpay.infra.container.RabbitMqContainer;
import org.testcontainers.containers.Network;

public class BasicInfra {
    private static RabbitMqContainer rabbitMqContainer = null;
    private static PostgresContainer postgresContainer = null;
    private static PicpayClientBoundaryRestContainer picpayClientBoundaryRestContainer = null;


    static{
        var network = Network.newNetwork();

        rabbitMqContainer = new RabbitMqContainer();
        rabbitMqContainer.withNetwork(network);
        rabbitMqContainer.start();

        postgresContainer = new PostgresContainer();
        postgresContainer.withNetwork(network);
        postgresContainer.start();

        picpayClientBoundaryRestContainer = new PicpayClientBoundaryRestContainer();
        picpayClientBoundaryRestContainer.withNetwork(network);
        picpayClientBoundaryRestContainer.start();
    }

    public static RabbitMqContainer getRabbitMqContainer() {
        return rabbitMqContainer;
    }

    public static PostgresContainer getPostgresContainer() {
        return postgresContainer;
    }

    public static PicpayClientBoundaryRestContainer getPicpayClientBoundaryRestContainer() {
        return picpayClientBoundaryRestContainer;
    }
}
