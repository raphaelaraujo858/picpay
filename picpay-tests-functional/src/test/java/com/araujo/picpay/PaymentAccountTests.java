package com.araujo.picpay;

import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpStatus.*;

class PaymentAccountTests extends BaseTests {

	private String defaultBarCode = "1234567890123456789012345678901234567890";

	@Test
	public void GivenAnActiveAccount_WhenDoAValidPayment_ThenDepositOccursWithSuccess() {

		// Arrange
		int amount = 100;
		String externalId = createExternalId();
		Date currentDate = new Date();

		var body = createPaymentAccountBody(amount, externalId, defaultBarCode, currentDate);

		// act
		var response = executePaymentAccountRequest(body, getActiveAccountNumber());

		// assert
		var responseStatusCode = response.get("status");
		var responseBody = (Map) response.get("body");

		assertEquals(ACCEPTED, responseStatusCode);
		assertEquals(amount, responseBody.get("amount"));
		assertEquals(externalId, responseBody.get("transactionExternalId"));
		assertEquals(getActiveAccountNumber(), responseBody.get("accountNumber"));
		assertEquals(formatDateToJsonPattern(currentDate) + "T00:00:00.000+00:00", responseBody.get("paymentDate"));
		assertEquals(defaultBarCode, responseBody.get("barCode"));
		assertTrue(responseBody.containsKey("id"));

		//TODO adicionar validações de informações no BD
	}

	@Test
	public void GivenAInactiveAccount_WhenDoAPayment_ThenTransactionShouldBeRefused() {

		// Arrange
		int amount = 100;
		String externalId = createExternalId();
		Date currentDate = new Date();
		String expectedErrorMessage = "Account is not active";
		var body = createPaymentAccountBody(amount, externalId, defaultBarCode, currentDate);

		// act
		var response = executePaymentAccountRequest(body, getInactiveAccountNumber());

		// assert
		var responseStatusCode = response.get("status");
		var responseBody = (Map) response.get("body");

		assertEquals(BAD_REQUEST, responseStatusCode);
		assertEquals(expectedErrorMessage, responseBody.get("error"));
	}

	@Test
	public void GivenAnAccount_WhenDoAPaymentWithInvalidBarCode_ThenTransactionShouldBeRefused() {

		// Arrange
		int amount = 100;
		String invalidBarCode = "abcabcabc";
		String externalId = createExternalId();
		String expectedErrorMessage = "size must be between 40 and 48";
		Date currentDate = new Date();
		var body = createPaymentAccountBody(amount, externalId, invalidBarCode, currentDate);

		// act
		var response = executePaymentAccountRequest(body, getActiveAccountNumber());

		// assert
		var responseStatusCode = response.get("status");
		var responseBody = (Map) response.get("body");

		assertEquals(BAD_REQUEST, responseStatusCode);
		assertEquals(expectedErrorMessage, responseBody.get("barCode"));
	}

	@Test
	public void GivenAnAccount_WhenDoAPaymentWithAmountAboveBalance_ThenTransactionShouldBeRefused() {

		// Arrange
		int amount = 99999;
		String externalId = createExternalId();
		String expectedErrorMessage = "There is no balance for withdrawal";
		Date currentDate = new Date();
		var body = createPaymentAccountBody(amount, externalId, defaultBarCode, currentDate);

		// act
		var response = executePaymentAccountRequest(body, getActiveAccountNumber());

		// assert
		var responseStatusCode = response.get("status");
		var responseBody = (Map) response.get("body");

		assertEquals(BAD_REQUEST, responseStatusCode);
		assertEquals(expectedErrorMessage, responseBody.get("error"));
	}

}
