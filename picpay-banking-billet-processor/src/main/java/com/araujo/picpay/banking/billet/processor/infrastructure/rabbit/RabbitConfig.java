package com.araujo.picpay.banking.billet.processor.infrastructure.rabbit;

import org.springframework.amqp.core.Declarables;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class RabbitConfig {

    @Value("${bankingbillet.queue.name}")
    private String bankingBilletQueueName;

    @Value("${bankingbillet.queue.durable}")
    private Boolean durable;

    @Value("${bankingbillet.queue.auto-delete}")
    private Boolean autoDelete;

    @Value("${bankingbillet.queue.x-delivery-limit}")
    private Integer deliveryLimit;

    @Value("${bankingbillet.queue.x-max-length}")
    private Integer maxLength;

    @Bean
    public Declarables declarables(){
        Queue bankingBilletQueue = new Queue(bankingBilletQueueName, durable, false, autoDelete,
                Map.of(
                        "x-delivery-limit", deliveryLimit,
                        "x-max-length", maxLength
                ));

        return new Declarables(bankingBilletQueue);
    }

}
