package com.araujo.picpay.banking.billet.processor.application;

import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class BankingBilletConsumer {

    private static Logger logger = LoggerFactory.getLogger(BankingBilletConsumer.class);

    @RabbitListener(queues = {"${bankingbillet.queue.name}"})
    public void receive(@Payload String payload){

        ThreadContext.clearAll();
        ThreadContext.put("payload", payload);
        logger.info("Starting receive");


    }
}