Desenvolva uma solução Web Backend para servir um aplicativo de carteira digital (Wallet), onde os usuários deste aplicativo realizam operações financeiras básicas como: Transferência de valores entre usuário, Saque, Depósito e Pagamento de contas. Considere que essa é uma solução baseada em microservice.


Você terá até 7 dias corridos para entregar após o recebimento.



Alguns pré-requisitos:

    Deverá ser considerado uma timeline com as movimentações financeiras que ocorreram na conta do usuário;
    Deverá implementar no mínimo uma comunicação assíncrona;
    Deverá ser desenvolvido os testes unitários do projeto;
    Será um diferencial o desenvolvimento de testes integrados;
    A aplicação deverá ser uma aplicação Web API REST;
    Deverá possuir persistência de dados;
    Deverá ser distribuída como container Docker;
    Será um diferencial a criação do desenho de arquitetura da solução considerando integração com um aplicativo Mobile e Web (incluir no README do projeto);
    Deverá ser disponibilizado em uma repositório Git como gitHub ou Bitbucket;
    Deverá conter um README.md com as instruções para executar o projeto localmente.
    Poderá ser utilizado as seguintes linguagens de programação: Java, Kotlin, GoLang.